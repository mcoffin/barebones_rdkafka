extern crate barebones_rdkafka;
extern crate env_logger;
extern crate signal_hook;

use barebones_rdkafka::{
    rdkafka,
    consumer::PartitionConsumer,
    topic::Topic,
    Offset,
    Message,
};

use rdkafka::client::{
    Client,
    DefaultClientContext,
};
use rdkafka::config::{
    ClientConfig,
};
use rdkafka::types::RDKafkaType;

use signal_hook::{
    iterator::Signals,
    SIGINT,
};

use std::io::{
    self,
    Write
};
use std::sync::{
    Arc,
    atomic::AtomicBool,
    atomic,
};
use std::thread;
use std::time;

const DEFAULT_TIMEOUT: time::Duration = time::Duration::from_millis(10000);

fn setup_signal_handling<T: AsRef<AtomicBool> + Send + 'static>(running: T) {
    let signals = Signals::new(&[SIGINT])
        .expect("Failed to set up signal handling");
    thread::spawn(move || {
        let running = running.as_ref();
        for _sig in signals.forever() {
            {
                let stderr = io::stderr();
                let mut handle = stderr.lock();
                let _ = writeln!(&mut handle, "Shutting down...");
            }
            running.store(false, atomic::Ordering::Relaxed);
        }
    });
}

fn main() {
    env_logger::init();

    let mut config = ClientConfig::new();
    config.set("bootstrap.servers", "localhost:9092");

    let client: Client<DefaultClientContext> = {
        let native_config = config.create_native_config()
            .expect("Failed to create native config");
        Client::new(
            &config,
            native_config,
            RDKafkaType::RD_KAFKA_CONSUMER,
            DefaultClientContext {}
        )
            .expect("Failed to create client")
    };
    let topic: Topic<DefaultClientContext, _> = Topic::new(&client, "test-topic", Default::default());
    let mut consumer = PartitionConsumer::new(topic, 0, Offset::Beginning)
        .expect("Failed to create consumer");
    let running = Arc::new(AtomicBool::new(true));
    setup_signal_handling(running.clone());
    while running.load(atomic::Ordering::Relaxed) {
        let message = consumer.consume(DEFAULT_TIMEOUT)
            .expect("Failed to consume");
        if let Some(message) = message {
            let payload = message.payload().and_then(|b| std::str::from_utf8(b).ok()).unwrap();
            println!("{}", payload);
        }
    }
}
