pub extern crate rdkafka;
pub extern crate rdkafka_sys;
#[macro_use]
extern crate log;
extern crate combination_err;

pub mod consumer;
pub mod topic;
// pub mod client;
pub mod config;
mod slice_util;

use combination_err::combination_err;

use rdkafka::error::{
    KafkaError,
    RDKafkaError,
};
use rdkafka_sys::bindings as sys;

use std::mem;
use std::ops::{
    Deref,
    DerefMut,
};
use std::ptr;

pub use rdkafka::topic_partition_list::Offset;

pub trait Message {
    fn payload(&self) -> Option<&[u8]>;
    // fn payload_mut(&self) -> Option<&mut [u8]>;
    fn key(&self) -> Option<&[u8]>;
    // fn key_mut(&self) -> Option<&mut [u8]>;
    fn error(&self) -> RDKafkaError;
}

impl Message for sys::rd_kafka_message_t {
    fn payload(&self) -> Option<&[u8]> {
        if self.payload.is_null() {
            return None;
        }
        unsafe {
            Some(slice_util::slice_from_void(self.payload, self.len))
        }
    }

    fn key(&self) -> Option<&[u8]> {
        if self.key.is_null() {
            return None;
        }
        unsafe {
            Some(slice_util::slice_from_void(self.key, self.key_len))
        }
    }

    #[inline(always)]
    fn error(&self) -> RDKafkaError {
        self.err.into()
    }
}

pub struct OwnedMessage(*mut sys::rd_kafka_message_t);

impl OwnedMessage {
    #[inline(always)]
    pub unsafe fn from_native_ptr(ptr: *mut sys::rd_kafka_message_t) -> OwnedMessage {
        OwnedMessage(ptr)
    }
}

impl Deref for OwnedMessage {
    type Target = sys::rd_kafka_message_t;

    fn deref(&self) -> &Self::Target {
        unsafe {
            self.0.as_ref().unwrap()
        }
    }
}

impl DerefMut for OwnedMessage {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe {
            self.0.as_mut().unwrap()
        }
    }
}

impl Drop for OwnedMessage {
    fn drop(&mut self) {
        trace!("OwnedMessage::drop");
        if !self.0.is_null() {
            unsafe {
                sys::rd_kafka_message_destroy(self.0);
            }
        }
        self.0 = ptr::null_mut();
    }
}

unsafe impl Send for OwnedMessage {}

pub struct MaybeOwnedMessage(Option<&'static mut sys::rd_kafka_message_t>);

impl MaybeOwnedMessage {
    pub fn get<'a>(&'a self) -> Option<&'a mut sys::rd_kafka_message_t> {
        let ret = self.0.as_ref().map(|r| *r as *const _);
        unsafe {
            ret.map(|p| mem::transmute(p))
        }
    }

    pub fn get_mut<'a>(&'a mut self) -> Option<&'a mut sys::rd_kafka_message_t> {
        let ret = self.0.as_mut().map(|r| *r as *const _);
        unsafe {
            ret.map(|p| mem::transmute(p))
        }
    }

    pub fn reset(&mut self) {
        let ptr = match &mut self.0 {
            &mut Some(ref mut r) => Some(*r as *mut _),
            &mut None => None,
        };
        if let Some(ptr) = ptr {
            unsafe {
                sys::rd_kafka_message_destroy(ptr);
            }
        }
        self.0 = None;
    }
}

impl From<OwnedMessage> for MaybeOwnedMessage {
    #[inline(always)]
    fn from(msg: OwnedMessage) -> MaybeOwnedMessage {
        unsafe {
            mem::transmute(msg)
        }
    }
}

impl Default for MaybeOwnedMessage {
    fn default() -> MaybeOwnedMessage {
        MaybeOwnedMessage(Default::default())
    }
}

impl Drop for MaybeOwnedMessage {
    fn drop(&mut self) {
        trace!("MaybeOwnedMessage::drop");
        self.reset();
    }
}

unsafe impl Send for MaybeOwnedMessage {}

/// Wrapper for `rd_kafka_last_error`
pub fn last_error() -> RDKafkaError {
    RDKafkaError::from(unsafe {
        sys::rd_kafka_last_error()
    })
}

/// Error type for an error that either came from `rdkafka` or `barebones_rdkafka`
#[combination_err("RDKafka error", "RDKafka error", "RDKafka error")]
#[derive(Debug)]
pub enum CombinedError {
    RDKafka(KafkaError),
    Barebones(RDKafkaError),
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem;
    #[test]
    fn owned_message_size() {
        assert_eq!(
            mem::size_of::<OwnedMessage>(),
            mem::size_of::<MaybeOwnedMessage>(),
        );
    }
}
