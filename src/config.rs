use rdkafka_sys::bindings as sys;
use std::ffi;
use std::ptr;

pub trait Conf {
    fn set(&mut self, name: &str, value: &str) -> Result<(), ConfError>;
}

pub enum ConfError {
    Unknown,
    Invalid,
}

impl ConfError {
    #[inline(always)]
    pub fn from_res(res: sys::rd_kafka_conf_res_t) -> Result<(), ConfError> {
        match res {
            sys::rd_kafka_conf_res_t::RD_KAFKA_CONF_OK => Ok(()),
            sys::rd_kafka_conf_res_t::RD_KAFKA_CONF_UNKNOWN => Err(ConfError::Unknown),
            sys::rd_kafka_conf_res_t::RD_KAFKA_CONF_INVALID => Err(ConfError::Invalid),
        }
    }
}

impl Conf for sys::rd_kafka_topic_conf_s {
    fn set(&mut self, name: &str, value: &str) -> Result<(), ConfError> {
        let c_name = ffi::CString::new(name).unwrap();
        let c_value = ffi::CString::new(value).unwrap();
        let res = unsafe {
            sys::rd_kafka_topic_conf_set(self as *mut sys::rd_kafka_topic_conf_s, c_name.as_ptr(), c_value.as_ptr(), ptr::null_mut(), 0)
        };
        ConfError::from_res(res)
    }
}
