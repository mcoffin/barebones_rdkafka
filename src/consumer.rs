use rdkafka::client::{
    Client,
    ClientContext,
};
use rdkafka::error::RDKafkaError;

use rdkafka_sys::bindings as sys;

use std::borrow::Borrow;
use std::marker::PhantomData;
use std::mem;
use std::time;

use crate::{
    last_error,
    MaybeOwnedMessage,
    Message,
    Offset,
    OwnedMessage,
};
use crate::topic::Topic;

pub struct PartitionConsumer<Ctx: ClientContext, C: Borrow<Client<Ctx>>, T: Borrow<Topic<Ctx, C>>> {
    partition: i32,
    topic: T,
    started: bool,
    _ctx: PhantomData<Ctx>,
    _client: PhantomData<C>,
}

impl<Ctx: ClientContext, C: Borrow<Client<Ctx>>, T: Borrow<Topic<Ctx, C>>> PartitionConsumer<Ctx, C, T> {
    pub fn new(topic: T, partition: i32, offset: Offset) -> Result<Self, RDKafkaError> {
        let mut ret = PartitionConsumer {
            partition: partition,
            topic: topic,
            started: false,
            _ctx: PhantomData {},
            _client: PhantomData {},
        };
        ret.consume_start(offset)?;
        Ok(ret)
    }

    fn consume_start(&mut self, offset: Offset) -> Result<(), RDKafkaError> {
        let status = unsafe {
            sys::rd_kafka_consume_start(self.topic.borrow().native_ptr(), self.partition, offset.to_raw())
        };
        if status < 0 {
            return Err(last_error());
        }
        self.started = true;
        Ok(())
    }

    fn consume_stop(&mut self) -> Result<(), RDKafkaError> {
        trace!("PartitionConsumer::consume_stop");
        let status = unsafe {
            sys::rd_kafka_consume_stop(self.topic.borrow().native_ptr(), self.partition)
        };
        if status < 0 {
            return Err(last_error());
        }
        self.started = false;
        Ok(())
    }

    pub fn consume(&mut self, timeout: time::Duration) -> Result<Option<OwnedMessage>, RDKafkaError> {
        let timeout = timeout.as_millis() as std::os::raw::c_int;
        let ptr = unsafe {
            sys::rd_kafka_consume(self.topic.borrow().native_ptr(), self.partition, timeout)
        };
        if ptr.is_null() {
            let ret = match last_error() {
                RDKafkaError::OperationTimedOut => Ok(None),
                e => Err(e),
            };
            return ret;
        }
        let msg = unsafe {
            OwnedMessage::from_native_ptr(ptr)
        };
        match msg.error() {
            RDKafkaError::NoError => {},
            RDKafkaError::OperationTimedOut => {
                return Ok(None)
            },
            e => {
                return Err(e)
            },
        }
        Ok(Some(msg))
    }

    pub fn consume_batch<'buf>(&mut self, timeout: time::Duration, buf: &'buf mut [MaybeOwnedMessage]) -> Result<&'buf [OwnedMessage], RDKafkaError> {
        let timeout = timeout.as_millis() as std::os::raw::c_int;
        for msg in buf.iter_mut() {
            msg.reset();
        }
        let processed = unsafe {
            sys::rd_kafka_consume_batch(
                self.topic.borrow().native_ptr(),
                self.partition,
                timeout,
                mem::transmute(buf.as_mut_ptr()),
                buf.len()
            )
        };
        if processed < 0 {
            return Err(last_error());
        }
        let processed = processed as usize;
        let ret = &mut buf[0..processed];
        Ok(unsafe {
            mem::transmute(ret)
        })
    }
}

impl<Ctx: ClientContext, C: Borrow<Client<Ctx>>, T: Borrow<Topic<Ctx, C>>> Drop for PartitionConsumer<Ctx, C, T> {
    fn drop(&mut self) {
        trace!("PartitionConsumer::drop");
        if self.started {
            let _ = self.consume_stop();
        }
    }
}

unsafe impl<Ctx: ClientContext, C: Borrow<Client<Ctx>>, T: Borrow<Topic<Ctx, C>>> Send for PartitionConsumer<Ctx, C, T> {}
