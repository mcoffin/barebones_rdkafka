#![allow(dead_code)]

use std::ffi;
use std::mem;
use std::slice;

pub unsafe fn slice_from_void<'a, T: Sized>(ptr: *const ffi::c_void, len: usize) -> &'a [T] {
    let ptr: *const T = mem::transmute(ptr);
    let len = len / mem::size_of::<T>();
    slice::from_raw_parts(ptr, len)
}

pub unsafe fn slice_from_void_mut<'a, T: Sized>(ptr: *mut ffi::c_void, mut len: usize) -> &'a mut [T] {
    let ptr: *mut T = mem::transmute(ptr);
    len = len / mem::size_of::<T>();
    slice::from_raw_parts_mut(ptr, len)
}
