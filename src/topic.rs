use rdkafka::client::{
    Client,
    ClientContext,
};
use crate::CombinedError;
use rdkafka_sys::bindings as sys;
use std::borrow::Borrow;
use std::ffi;
use std::marker::PhantomData;
use std::ops::{
    Deref,
    DerefMut
};
use std::ptr;

/// Owned version of `rd_kafka_topic_t`
pub struct Topic<Ctx: ClientContext, T: Borrow<Client<Ctx>>> {
    ptr: *mut sys::rd_kafka_topic_t,
    _client: T,
    _ctx: PhantomData<Ctx>,
}

impl<T: Borrow<Client<Ctx>>, Ctx: ClientContext> Topic<Ctx, T> {
    // TODO: find out if that mut is necessary for conf
    pub fn new(client: T, topic: &str, conf: TopicConfig) -> Self {
        let topic = ffi::CString::new(topic).unwrap();
        let ptr = unsafe {
            sys::rd_kafka_topic_new(client.borrow().native_ptr(), topic.as_ptr(), conf.leak() as *mut _)
        };
        Topic {
            ptr: ptr,
            _client: client,
            _ctx: PhantomData {},
        }
    }

    #[inline(always)]
    pub fn native_ptr(&self) -> *mut sys::rd_kafka_topic_t {
        self.ptr
    }
}

impl<T: Borrow<Client<Ctx>>, Ctx: ClientContext> Deref for Topic<Ctx, T> {
    type Target = sys::rd_kafka_topic_t;

    fn deref(&self) -> &Self::Target {
        unsafe {
            self.ptr.as_ref().unwrap()
        }
    }
}

impl<T: Borrow<Client<Ctx>>, Ctx: ClientContext> DerefMut for Topic<Ctx, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe {
            self.ptr.as_mut().unwrap()
        }
    }
}

impl<T: Borrow<Client<Ctx>>, Ctx: ClientContext> Drop for Topic<Ctx, T> {
    fn drop(&mut self) {
        trace!("Topic::drop");
        unsafe {
            sys::rd_kafka_topic_destroy(self.ptr);
            self.ptr = ptr::null_mut();
        }
    }
}

unsafe impl<Ctx: ClientContext, T: Borrow<Client<Ctx>>> Send for Topic<Ctx, T> {}

/// Owned version of `rd_kafka_topic_conf_t`
pub struct TopicConfig(*mut sys::rd_kafka_topic_conf_t);

impl TopicConfig {
    pub fn new() -> TopicConfig {
        let ptr = unsafe {
            sys::rd_kafka_topic_conf_new()
        };
        TopicConfig(ptr)
    }

    /// Leaks the native pointer. The caller assumes responsibility for freeing it.
    pub unsafe fn leak(mut self) -> *mut sys::rd_kafka_topic_conf_t {
        let ret = self.0;
        self.0 = ptr::null_mut();
        ret
    }

    #[inline(always)]
    pub unsafe fn from_native_ptr(ptr: *mut sys::rd_kafka_topic_conf_t) -> TopicConfig {
        TopicConfig(ptr)
    }
}

impl Deref for TopicConfig {
    type Target = sys::rd_kafka_topic_conf_t;
    fn deref(&self) -> &Self::Target {
        unsafe {
            self.0.as_ref().unwrap()
        }
    }
}

impl DerefMut for TopicConfig {
    fn deref_mut(&mut self) -> &mut sys::rd_kafka_topic_conf_t {
        unsafe {
            self.0.as_mut().unwrap()
        }
    }
}

impl Default for TopicConfig {
    fn default() -> Self {
        TopicConfig::new()
    }
}

impl Clone for TopicConfig {
    fn clone(&self) -> Self {
        unsafe {
            let ptr = sys::rd_kafka_topic_conf_dup(self.0 as *const sys::rd_kafka_topic_conf_t);
            TopicConfig::from_native_ptr(ptr)
        }
    }
}

impl Drop for TopicConfig {
    fn drop(&mut self) {
        if !self.0.is_null() {
            unsafe {
                sys::rd_kafka_topic_conf_destroy(self.0);
            }
        }
        self.0 = ptr::null_mut();
    }
}

unsafe impl Send for TopicConfig {}
